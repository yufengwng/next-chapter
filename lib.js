const $ = require('jquery');
const Storage = require('./storage');
const nc = {};

nc.initStorage = (type) => {
  try {
    let storage = window[type];
    let x = '__storage_test__';
    storage.setItem(x, x);
    storage.removeItem(x);
    nc.store = storage;
  } catch(e) {
    nc.store = new Storage();
  }
}

nc.loadColoredCache = () => {
  try {
    let cache = {};
    let html = $(document.body).html();
    let content = html.split('\n').join('').match(/<table (.*)<\/table>/)[1];
    let links = content.split('</a>').join('\n').match(/href="\/Manga.*" title/g);

    links.map(href => {
      let url = href.match(/"(.*\?id=\d{6})"/)[1];
      let ch = nc.getChapter(url);
      cache[ch] = url;
    });

    cache = JSON.stringify(cache);
    nc.store.setItem('nc.colored-cache', cache);
    nc.store.setItem('nc.colored-timestamp', Date.now());
  } catch (err) {
    console.error('Failed caching for colored chapters');
    console.error(err);
  }
}

nc.loadMangaCache = () => {
  try {
    let cache = {};
    let html = $(document.body).html();
    let content = html.split('\n').join('').match(/<table (.*)<\/table>/)[1];
    let links = content.split('</a>').join('\n').match(/href="\/Manga.*" title/g);

    links.map(href => {
      let url = href.match(/"(.*\?id=\d{6})"/)[1];
      let ch = nc.getChapter(url);
      cache[ch] = url;
    });

    cache = JSON.stringify(cache);
    nc.store.setItem('nc.manga-cache', cache);
    nc.store.setItem('nc.manga-timestamp', Date.now());
  } catch (err) {
    console.error('Failed caching for manga chapters');
    console.error(err);
  }
}

nc.checkColoredCache = () => {
  let oneDay = 1000 * 60 * 60 * 24;
  let now = Date.now();

  let timestamp = nc.store.getItem('nc.colored-timestamp');
  if (!timestamp || now - timestamp > oneDay) {
    nc.loadColoredCache();
  }
}

nc.checkMangaCache = () => {
  let oneDay = 1000 * 60 * 60 * 24;
  let now = Date.now();

  timestamp = nc.store.getItem('nc.manga-timestamp');
  if (!timestamp || now - timestamp > oneDay) {
    nc.loadMangaCache();
  }
}

nc.goto = (url) => {
  let a = document.createElement('a');
  a.setAttribute('href', url);
  a.setAttribute('id', 'nc-anchor');
  document.body.appendChild(a);
  document.getElementById('nc-anchor').click();
}

nc.isColored = (url) => {
  return url.match(/.*Digital-Colored-Comics.*/);
};

nc.getChapter = (url) => {
  let colored = /Ch-(\d{3}).*\?/;
  let manga = /(?:Chapter-)?(\d{3}(?:-\d{3})?).*\?/;

  let found = url.match(colored);
  if (found) return parseInt(found[1]);

  found = url.match(manga);
  if (found) return parseFloat(found[1].replace('-', '.'));

  return null;
};

nc.getMangaUrl = (ch, callback) => {
  let cache = nc.store.getItem('nc.manga-cache');
  if (cache) {
    cache = JSON.parse(cache);
    let path = cache[ch] || null;
    if (path) {
      let km = $(location).attr('origin');
      callback(km + path);
    } else {
      callback(null);
    }
  } else {
    callback(null);
  }
};

nc.getColoredUrl = (ch, callback) => {
  let cache = nc.store.getItem('nc.colored-cache');
  if (cache) {
    cache = JSON.parse(cache);
    let path = cache[ch] || null;
    if (path) {
      let km = $(location).attr('origin');
      callback(km + path);
    } else {
      callback(null);
    }
  } else {
    callback(null);
  }
};

module.exports = nc;
