class Storage {
  constructor() {
    this._cache = {};
  }

  setItem(key, val) {
    this._cache[key] = val;
  }

  getItem(key) {
    return this._cache[key];
  }

  removeItem(key) {
    delete this._cache[key];
  }

  clear() {
    this._cache = {};
  }
}

module.exports = Storage;
