const $ = require('jquery');
const nc = require('./lib.js');

function registerPreviousButton(ch) {
  $('img.btnPrevious').parent().click((event) => {
    event.preventDefault();
    let prevUrl = $('img.btnPrevious').parent().attr('href');
    let prevCh = nc.getChapter(prevUrl);
    let chapter = ch - 1;
    if (chapter < 0) chapter = 0;

    if (chapter === prevCh) {
      if (nc.isColored(prevUrl)) {
        nc.goto(prevUrl);
      } else {
        nc.getColoredUrl(prevCh, (colored) => {
          if (colored) {
            let msg = `Previous chapter ${prevCh} is available in color. Want it?`;
            if (confirm(msg)) {
              nc.goto(colored);
            } else {
              nc.goto(prevUrl);
            }
          } else {
            nc.goto(prevUrl);
          }
        });
      }
    } else {
      nc.getColoredUrl(chapter, (colored) => {
        if (colored) {
          nc.goto(colored);
        } else {
          let msg = `Previous chapter ${chapter} is not available in color. Switching to original manga.`;
          alert(msg);
          nc.getMangaUrl(chapter, (manga) => {
            if (manga) {
              nc.goto(manga);
            } else {
              let msg = `Unable to find original manga for chapter ${chapter}.`;
              alert(msg);
            }
          });
        }
      });
    }
  });
}

function registerNextButton(ch) {
  $('img.btnNext').parent().click((event) => {
    event.preventDefault();
    let nextUrl = $('img.btnNext').parent().attr('href');
    let nextCh = nc.getChapter(nextUrl);
    let chapter = ch + 1;

    if (chapter === nextCh) {
      if (nc.isColored(nextUrl)) {
        nc.goto(nextUrl);
      } else {
        nc.getColoredUrl(nextCh, (colored) => {
          if (colored) {
            let msg = `Next chapter ${nextCh} is available in color. Want it?`;
            if (confirm(msg)) {
              nc.goto(colored);
            } else {
              nc.goto(nextUrl);
            }
          } else {
            nc.goto(nextUrl);
          }
        });
      }
    } else {
      nc.getColoredUrl(chapter, (colored) => {
        if (colored) {
          nc.goto(colored);
        } else {
          let msg = `Next chapter ${chapter} is not avilable in color. Switching to original manga.`;
          alert(msg);
          nc.getMangaUrl(chapter, (manga) => {
            if (manga) {
              nc.goto(manga);
            } else {
              let msg = `Unable to find original manga for chapter ${chapter}.`;
              alert(msg);
            }
          });
        }
      });
    }
  });
}

$(document).ready(() => {
  let url = $(location).attr('href') || "";
  nc.initStorage('localStorage');

  if (url.match(/\/Manga\/One-Piece\/?$/)) {
    nc.checkMangaCache();
  } else if (url.match(/\/Manga\/One-Piece-Digital-Colored-Comics\/?$/)) {
    nc.checkColoredCache();
  } else {
    let ch = nc.getChapter(url);

    // Offer to switch to colored chapter.
    if (!nc.isColored(url)) {
      nc.getColoredUrl(ch, (colored) => {
        if (colored) {
          let msg = `Chapter ${ch} is available in color. Switch?`;
          if (confirm(msg)) nc.goto(colored);
        }
      });
    }

    registerPreviousButton(ch);
    registerNextButton(ch);
  }
});
