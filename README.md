# Next Chapter

Chrome extension that offers to fetch the colored version of One Piece
chapters on KissManga.

## Getting Started

First clone the repo and then install dependencies:

```bash
$ git clone <repo>
$ cd /path/to/repo
$ npm install
```

## Build

The extension needs to be built first before installing:

```bash
$ npm run bundle
```

## Install

Manually use Developer Mode in Chrome to install the extension:

1. In Chrome, go to [chrome://extensions](chrome://extensions).
2. Check the `Developer mode` box.
3. Select `Load unpacked extension`.
4. Select this repo.

## Uninstall

1. In Chrome, go to [chrome://extensions](chrome://extensions).
2. Find this extension.
3. Click the trash icon.
4. Confirm by selecting `Remove`.
